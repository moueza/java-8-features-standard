package lambdasAdditions;

import java.util.List;

/**
 * https://www.oracle.com/webfolder/technetwork/tutorials/obe/java/Lambda-QuickStart/index.html#section5
 */
public class Person {
	int age;
private	Gender gender;
	private String givenName;
	private String surName;
	private String eMail;
	private String phone;
	private String address;
	public static List<Person> createShortList(){
		     List<Person> people = new ArrayList<>();
		     
		     people.add(
		       new Person.Builder()
		             .givenName("Bob")
		             .surName("Baker")
		             .age(21)
		             .gender(Gender.MALE)
		             .email("bob.baker@example.com")
		             .phoneNumber("201-121-4678")
		             .address("44 4th St, Smallville, KS 12333")
		             .build() 
		       );
 		     people.add(
		       new Person.Builder()
		             .givenName("Jane")
		             .surName("Doe")
		             .gender(Gender.FEMALE)
		             .email("jane.doe@example.com")
		             .phoneNumber("202-123-4678")
		             .address("33 3rd St, Smallville, KS 12333")
		             .build() 
		       );
		     
		     people.add(
		       new Person.Builder()
		             .givenName("John")
		             .surName("Doe")
		             .age(25)
		             .gender(Gender.MALE)
		             .email("john.doe@example.com")
		             .phoneNumber("202-123-4678")
		             .address("33 3rd St, Smallville, KS 12333")
		             .build()
		     );

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}



	public Object printWesternName() {
		// TODO Auto-generated method stub
		return null;
	}

	public char[] printCustom(Object object) {
		// TODO Auto-generated method stub
		return null;
	}
}
