package lambdasLooping;

import java.util.List;

import lambdasAdditions.Person;

public class Test01ForEach {
/**https://www.oracle.com/webfolder/technetwork/tutorials/obe/java/Lambda-QuickStart/index.html#section5*/
	public static void main(String[] args) {

		List<Person> pl = Person.createShortList();

		System.out.println("\n=== Western Phone List ===");
		pl.forEach(p -> p.printWesternName());

		System.out.println("\n=== Eastern Phone List ===");
		pl.forEach(Person::printEasternName);

		System.out.println("\n=== Custom Phone List ===");
		pl.forEach(p -> {
			System.out.println(p.printCustom(r -> "Name: " + r.getGivenName() + " EMail: " + r.getEmail()));
		});

	}
}